var mongoose = require('mongoose');

var PictureSchema = new mongoose.Schema({
    picture: String,
}, { versionKey: false });

module.exports = mongoose.model('Picture', PictureSchema);