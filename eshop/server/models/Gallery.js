var mongoose = require('mongoose');

var GallerySchema = new mongoose.Schema({
  place: String,
  picture: String,

}, { versionKey: false });

module.exports = mongoose.model('Gallery', GallerySchema);