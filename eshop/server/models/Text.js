var mongoose = require('mongoose'); 

const TextSchema = new mongoose.Schema({
    place: String,
    text: String
}, {versionKey: false});

module.exports = mongoose.model('Text', TextSchema);

