const mongoose = require('mongoose');

const fixedParcelSchema = new mongoose.Schema({
    name: String, 
    city: String, 
    address: String, 
    country: String, 
    postalcode: String, 
    routingcode: String, 
    availability: String, 
    description: String, 
    lat: Number,
    lng: Number,
    place_id: Number

}, {versionKey: false}); 

module.exports = mongoose.model('fixedParcel', fixedParcelSchema);