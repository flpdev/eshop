const mongoose = require('mongoose');

const ParcelSchema = new mongoose.Schema({
    name: String, 
    city: String, 
    address: String, 
    country: String, 
    postalcode: String, 
    routingcode: String, 
    availability: String, 
    description: String, 
    lat: [],
    lng: [],
    place_id: []

}, {versionKey: false}); 

module.exports = mongoose.model('Parcel', ParcelSchema);