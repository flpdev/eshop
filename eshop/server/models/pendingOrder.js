const mongoose = require('mongoose');
const Parcel = require('../models/Parcel')

// const ProductSchema = new mongoose.Schema({
//     name: {type: String, default: null},
//     shortDesc: String,
//     longDesc: String,
//     mainImage: String,
//     images: [String],
//     video: String, // url?
//     size: String, //small, medium, lerge, etc..
//     features: [String], 
//     materials: [String],
//     quantity: Number,
//     price: Number,
//     discountPrice: Number,
//     stockInfo: Number,
//     similarProducts: [String],
//     alternativeProducts: [String],
//     countOnOrder: {type: Number, default: 1}
    

// }, { versionKey: false });
const ProductSchema = new mongoose.Schema({
    _id: String, 
    countOnOrder: {type: Number, default: 1}
}, { versionKey: false });
const ParcelSchema = new mongoose.Schema({
    name: String, 
    city: String, 
    address: String, 
    country: String, 
    postalcode: String, 
    routingcode: String, 
    availability: String, 
    description: String, 
    lat: [],
    lng: [],
    place_id: []

}, {versionKey: false}); 

const pendingOrderSchema = new mongoose.Schema({
    orderID: String,
    address: {type: String, maxlength: 50},
    city: String,
    date: Date,
    name: String,
    lastname: String,
    products: [ProductSchema], 
    ZIPCode: {type: String, match: '^\d{5}$'}, //REGEX 5 digits, estonia zip is 5 digits
    details: String,
    phone:  String, 
    totalPrice: Number,
    email: String, 
    status: String, 
    paymentMethod: String, 
    sendingMethod: String, 
    parcel: ParcelSchema



}, {versionKey: false});







module.exports = mongoose.model('pendingOrder', pendingOrderSchema);