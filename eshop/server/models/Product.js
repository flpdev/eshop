const mongoose = require('mongoose');

const ProductSchema = new mongoose.Schema({
    name: {type: String, default: null},
    shortDesc: String,
    longDesc: String,
    mainImage: String,
    images: [String],
    video: String, // url?
    size: String, //small, medium, lerge, etc..
    features: [String], 
    materials: [String],
    quantity: Number,
    price: Number,
    discountPrice: Number,
    stockInfo: Number,
    similarProducts: [String],
    alternativeProducts: [String],
    countOnOrder: {type: Number, default: 1},
    videoEmbed: String

}, { versionKey: false }); /* If versionKey is needed - put back */

module.exports = mongoose.model('Product', ProductSchema);

