const mongoose = require('mongoose'); 

const AdminPicureSchema = new mongoose.Schema({
    picture: String, 
    place: String
}, {versionKey: false}); 

module.exports = mongoose.model('AdminPicture', AdminPicureSchema);