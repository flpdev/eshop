const mongoose = require('mongoose');

const PaymentReturnSchema = new mongoose.Schema({
    amount: Number,
    currency: String, 
    reference: String, // reference in Maksekeskus api
    shop: String, 
    transaction: String,
    status: String, 
    signature: String, //IGNORED IN THE API
    message_time: String,  //maybe Date?
    message_type: String, 
    customer_name: String
}, {versionKey: false});

module.exports = mongoose.model('PaymentReturn', PaymentReturnSchema);

