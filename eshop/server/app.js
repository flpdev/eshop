var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var logger = require('morgan');
var router = express.Router(); /* Added */
var mongoose = require('mongoose');
const config = require('./config/config');


const users = require('./routes/users')
const product = require('./routes/product')
const order = require('./routes/order')
const picture = require('./routes/picture')
const paymentreturn = require('./routes/PaymentReturn');
const parcels = require('./routes/getParcels')
const pendingOrder = require('./routes/pendingOrder')
const adminPicture = require('./routes/adminPicture');
const text = require('./routes/text');


var app = express();


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({'extended':'false'}));
app.use(express.static(path.join(__dirname, '../dist')));



//Connect to database
mongoose.connect(config.database, { useNewUrlParser: true });

//On connection
mongoose.connection.on('connected', ()=>{
    console.log('Connected to database ' + config.database);});
    // load parcels
    // parcels.getParcels();
// On Database Error
mongoose.connection.on('error', (err) =>{
    console.log('Database error: '+err);
});
app.use(logger(':date[clf] :method :url :status :response-time ms - :res[content-length]'));

// app.use('/api', router);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({'extended':'false'}));
app.use(express.static(path.join(__dirname, '../dist/eshop')));

app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');

/* Added Access origin, headers & methods */
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', "*");
  res.header('Access-Control-Allow-Methods','GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
}); 

/* Added */

app.use('/users', users);
app.use('/product', product)
app.use('/order', order);
app.use('/picture', picture);
app.use('/paymentreturn', paymentreturn);
app.use('/parcels', parcels)
app.use('/pendingorder', pendingOrder)
app.use('/adminPicture', adminPicture)
app.use('/text', text)
app.use('/uploads', express.static(__dirname + '../../src/assets/uploads'));



// Send all other requests to the Angular app
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '../dist/eshop/index.html'));
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
