module.exports = {
    database: 'mongodb://localhost:27017/eshop', /* insert here your mongodb connection default is set*/
    secret: 'yoursecret', /* Secret for log in */
    domain: 'domain', /* Mailgun domain for booking mails */ 
    apikey: 'key', /* Api key from Mailgun also foor booking mails */
    port: '3000' /* Can change your port here */
} /* Place this file to server/config folder and rename it to config.js */
