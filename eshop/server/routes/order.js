var express = require('express');
var router = express.Router();
var Order = require('../models/Order.js')
var moment = require('moment');
var moment = require('moment-timezone');

/* Mailgun connection */
//var mailgun = require('mailgun-js')({apiKey: config.apikey, domain: config.domain});

//GET ALL ORDERS
router.get('/', function(req, res, next){
    Order.find(function(err, orders){
        if(err) return next(err); 
        res.json(orders)
    });
});

router.get('/oneOrder/:id', function(req, res, next){
    Order.findOne({orderID: req.params.id}, function(err, order){
        if(err) return next(err); 
        res.json(order);
    })
})

router.get('/oneOrderReg/:id', function(req, res, next){
    Order.find({orderID: {$regex: "^.*" + req.params.id + ".*$", $options: 'i'}}, function(err, order){
        if(err) return next(err); 
        res.json(order);
    })
})

//SAVE ORDER

router.post('/save', function(req, res, next){
    moment.locale('et');
    req.body.date = moment.tz(req.body.dateStr, 'Europe/Tallinn');
    Order.countDocuments({orderID: req.body.orderID}, function(err, count){
        if(count==0){
            var htmlbuilder = ""; 
            var products = req.body.products;
            for(let i=0; i<products.lenght; i++){
                console.log(i)
                console.log(products[i]);
                htmlbuilder += "<tr><td><b>Item name:</b> "+ products[i]['name']+" , <b>Item quantity:</b> "+ products[i]['countOnOrder'] +"</td></tr>";
            }

            var toCustomer ={
                from: '',
                to: req.body.email, 
                subject: 'Order confirmation', 
                html: '<!DOCTYPE html> \
                <html>\
                <head>\
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />\
                <title>Order confirmation</title>\
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>\
                </head>\
                <body style="margin: 0; padding: 0;">\
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" style="border:none;">\
                    <tr><td style="color:#1a5733; font-size:24px;">Hello, '+ req.body.name +'</td></tr>\
                    <tr><td style="color:#1a5733; font-size:24px;padding-top: 50px; padding-bottom: 50px;">Your order with ID: '+ req.body.orderID +' has been confirmed.</td></tr>\
                    <tr><td  style="color:#1a5733; font-size:24px;"><b>Products:</b></td></tr>\
                    '+htmlbuilder+'\
                    <tr><td style="padding-bottom: 30px"></td></tr>\
                    <tr><td style="color:#1a5733; font-size:24px;padding-top:50px;">Have a nice day</td></tr>\
                    <tr><td style="color:#1a5733; font-size:24px;">Eshop</td></tr>\
                </table>\
                </body>\
                </html>'
            }; 
            var toShop ={
                from: '',
                to: '', 
                subject: '', 
                html:
                '<!DOCTYPE html>\
                    <html>\
                    <head>\
                        <meta http-equiv="Content-Type" content="text/html; chraset=UTF-8"/>\
                        <title>Uus tellimus</title>\
                        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>\
                    </head>\
                    <body style="margin:0; padding:0;">\
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" style="border:none;">\
                        <tr><td style="padding-bottom:30px; font-size: 26px;">New order recieved</td></tr>\
                        <tr><td style="padding-bottom:30px;">Order information:</td></tr>\
                        <tr><td><b>Order ID:</b> '+req.body.orderID+'</td></tr>\
                        <tr><td><b>Customer name:</b> '+req.body.name+'</td></tr>\
                        <tr><td><b>Customer lastname:</b> '+req.body.lastname+'</td></tr>\
                        <tr><td><b>Customer email:</b> '+req.body.email+'</td></tr>\
                        <tr><td><b>Customer phone:</b> '+req.body.phone+'</td></tr>\
                        <tr><td><b>Address:</b> '+req.body.address+'</td></tr>\
                        <tr><td><b>City:</b> '+req.body.city+'</td></tr>\
                        <tr><td><b>ZIP code:</b> '+req.body.ZIPcode+'</td></tr>\
                        <tr><td style="padding-bottom: 30px;"><b>Details:</b> '+req.body.details+'</td></tr>\
                        <tr><td style="padding-bottom: 30px;"><b>Products:</b></td></tr>\
                        '+htmlbuilder+'\
                        <tr><td style="padding-bottom: 30px"></td></tr>\
                    </table>\
                    </body>\
                    </html>'
            }; 

            //  ----------- IF PAYMENT IS ACCEPTED ---------------------- Uncoment once site is online, and domain validated in mailgun

            
            // mailgun.messages().send(toCustomer, function (err, body) {
            //     if (err) return next(err);
            //       console.log(body);
            //   });
            //   mailgun.messages().send(toShop, function(err, body){
            //     if (err) return next(err);
            //     console.log(body);
            //   });

            Order.create(req.body, function(err, order){
                
                if(err) return next(err)
                res.json(order);
            });
        }else{
            console.log("Product exists");
        }
    })
    
});

//DELETE ORDER
router.delete('/:_id', function(req, res, next){
    Order.findByIdAndDelete(req.params._id, req.body, function(err, post){
        if(err) return next(err); 
        res.json(post);
    })
})


module.exports = router;