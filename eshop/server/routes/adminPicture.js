var express = require('express');
var router = express.Router();
var AdminPicture = require('../models/AdminPicture');
var fs = require('fs');


//ADD PICTURE
  router.post('/add', function(req, res, next){
    AdminPicture.countDocuments({
        place: req.body.place
    }, function(err, countDocuments){
        if(countDocuments==0){
            AdminPicture.create({
                place: req.body.place,
                picture: req.body.picture
            }, function(err, newItem){
                if(err) res.send(err);
                res.json(newItem);
            })
        }else{
            AdminPicture.findOne({
                place: req.body.place
            }, function(err, adminpicture){
                if(err) res.send(err)
                const filename = adminpicture.picture.split('/')[1];
                console.log(adminpicture.picture)
                console.log(filename)
                fs.unlink('./src/assets/uploads/'+filename, (err)=>{
                    if(err) console.log(err); 
                    console.log("old picture removed: " + filename);
                }); 
                
                adminpicture.update({
                    picture: req.body.picture
                }, function(err){
                    if(err) res.send(err);
                    res.json(adminpicture);
                });
            });
        }
    });
  });


  //GET PICTURE FROM PLACE
  router.post('/get', function(req, res){
      AdminPicture.countDocuments({
          place: req.body.place
      }, function(err, countDocuments){
          if(countDocuments==0){
              res.json({
                  msg: "There is no item in the DB"
              });
          }else{
              AdminPicture.findOne({
                  place: req.body.place
              }, function(err, adminpicture){
                  if(err) res.send(err); 
                  res.json(adminpicture);
              });
          }
      });
  });

  //GET ALL CONTENT PICTURES
  router.get('/getall', function(req, res, next){
      AdminPicture.find(function(err, adminpictures){
          if(err) return next(err); 
          res.json(adminpictures)
      });
  });



  module.exports = router;
