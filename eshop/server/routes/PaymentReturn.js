var express = require('express');
var router = express.Router();
var paymentReturn = require('../models/PaymentReturn')

//SAVE PAYMENT RETURN 
router.post('/add', function(req, res, next){
    paymentReturn.create(req.body, function(err, post){ //https://maksekeskus.ee/api-explorer/messages.php
        if(err) return next(err); 
        res.json(post);
    });
}); 


//GET PAYMENT RETURN BY ORDERID
router.get('/get', function(req, res, next){
    paymentReturn.findOne({
       reference: req.body.orderID 
    }, function(err, post){
        if(err) return next(err);
        res.json(post);
    });
});

module.exports = router;