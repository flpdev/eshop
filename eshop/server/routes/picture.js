var express = require('express');
var router = express.Router();
var Picture = require('../models/Picture');
var multer = require('multer');
var path = require('path');
var fs = require('fs');

var finalname;

const fileFilter = (req, file, cb) =>{
  if(file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpg' || file.mimetype === 'image/png' ){
    cb(null, true);
  }else {
    cb(null, false);
  }
};

var storage = multer.diskStorage({
  destination: path.join(__dirname, '../../src/assets/uploads'),
  filename: function(req, file, cb){
    var extArray = file.mimetype.split('/');
    var extension = extArray[extArray.length-1];
    cb(null, file.fieldname + Date.now()+ '.'+extension);
  }
})

var upload = multer({storage: storage, fileFilter: fileFilter});


/* GET ALL Pictures */
router.post('/', function(req, res, next) {
    Picture.find(function (err, pic) { 
      if (err) return next(err);
      res.json(pic);
    });
});

//ADD PICTURE
router.post('/add', upload.single('picture'), function(req, res, next) {
  var str = req.file.path;
    if(str.indexOf('/')>=0 && str.indexOf('/') != -1){
        var splitstr = str.split("/");
    } else {
        var splitstr = str.split(/(\u005C)/g);
    }
    var correctPath = 'uploads/'+splitstr[splitstr.length-1];


    var pic = {picture: correctPath} // no need to save picture in DB(?)
    res.json(pic);
    
  // Picture.create({picture: correctPath}, function(err,post) {
  //   if (err) return next(err);
  //   res.json(post);
  // });
});

//PREVIOUS CODE

// var storage = multer.diskStorage({
//   destination: path.join(__dirname, '../../src/assets/uploads'),
//   filename: function(req, file, cb){
//     var extArray = file.mimetype.split('/');
//     var extension = extArray[extArray.length-1];
//     cb(null, file.fieldname + Date.now()+ '.'+extension);
//   }
// })
// var upload = multer({storage: storage});


// /* GET ALL Pictures */
// router.post('/', function(req, res, next) {
//     Picture.find(function (err, pic) { 
//       if (err) return next(err);
//       res.json(pic);
//     });
// });

// router.post('/add', upload.single('file'), function(req, res, next) {
//   var str = req.file.path;
//     if(str.indexOf('/')>=0 && str.indexOf('/') != -1){
//         var splitstr = str.split("/");
//     } else {
//         var splitstr = str.split(/(\u005C)/g);
//     }
//     var correctPath = '/assets/uploads/'+splitstr[splitstr.length-1];
//   Picture.create({picture: correctPath}, function(err,post) {
//     if (err) return next(err);
//     res.json(post);
//   });
// });
// // //ADD PICTURE
// // router.post('/add', upload.single('file'), function(req, res, next) {
// //   console.log(req.file);
  
// //   Picture.create({picture: 'assets/uploads/' +finalname}, function(err,post) {
// //     if (err) return next(err);
// //     res.json(post);
// //   });
// // });



// //DELETE PICTURE
// router.delete('/delete/:_id', function(req, res, next){
//   Picture.findByIdAndRemove(req.params._id, req.body, function(err, post){
//     res.json(post);
//     var filename = post.picture.split('/')[2];
//     fs.unlink('../../src/assets/uploads/'+ filename, (err)=>{
//       if (err) console.log(err);
//       console.log("picture deleted: "+filename);

//     });
//   })
// })


// router.delete('/delete', function(req, res, next){
//   for(let i=0; i<req.body.images.length; i++){
//     fs.unlink(req.body.images[i], function(err){
//       if(err) return next(err); 
//       console.log("Image deleted: ", req.body.images[i].)
//     })
//   }
// })

module.exports = router;