var parcels = require('./parcels');
var express = require('express');
var router = express.Router();
var parcel = require('../models/Parcel')

router.get('/', function(req, res, next){
    parcels.getParcels(); 

    parcel.find(function(err, post){
        if(err) return next(err);
        res.json(post)
        
    })
})

router.post('/one', function(req, res, next){
    parcel.find({name : req.body.id}, function(err, parcel){
        if(err) return next(err); 
        res.json(parcel);
    })
})

module.exports = router;

