var express = require('express');
var router = express.Router();
var Order = require('../models/pendingOrder.js')


//GET ALL ORDERS
router.get('/', function(req, res, next){
    Order.find(function(err, orders){
        if(err) return next(err); 
        res.json(orders)
    });
});

//SAVE ORDER
router.post('/save', function(req, res, next){
    Order.countDocuments({orderID: req.body.orderID}, function(err, count){
        if(count==0){
            Order.create(req.body, function(err, order){
                if(err) return next(err)
                res.json(order);
            });
        }else{
            console.log("Document exists already");
        }
    })

});

//GET ORDER BY ID
router.get('/:_id', function(req, res, next){
    Order.findOne({orderID: req.params._id}, function(err, order){
        if(err) return next(err); 
        res.json(order)
    });
});

//DELETE ORDER
router.delete('/delete/:_id', function(req, res, next){
    Order.findOneAndDelete({orderID: req.params._id}, function(err, post){
        if(err) return next(err); 
        res.json(post);
    })
})


module.exports = router;