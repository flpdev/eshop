var express = require('express');
var router = express.Router();
var Product = require('../models/product.js');
var multer = require('multer'); 
var path = require('path');
var fs = require('fs')

var storage = multer.diskStorage({
    destination: path.join(__dirname, '../../src/assets/uploads/'), 
    filename: function(req, file, cb){
        var extArray = file.mimetype.split('/');
        var extension = extArray[extArray.length-1];
        cb(null, file.originalname)
    }
})
var upload = multer({storage: storage})


//GET ALL PRODUCTS
router.get('/getall/', function(req, res, next){
    Product.find(function(err, prodcuts){
        if(err) return next(err); 
        res.json(prodcuts);
    });
});

//GET ONE PRODUCT BY ID
router.get('/:_id', function(req, res, next){
    Product.findById(req.params._id, function(err, product){
        if(err) return next(err); 
        res.json(product)
    });
});

//GET PRODUCTS BY NAME
router.post('/byname', function(req, res, next){
    Product.find({"name": {$regex: "^.*" + req.body.key + ".*$", $options: 'i'}}, function(err, post){
        if(err) return next(err); 
        res.json(post);
    })
})

//^.*DEF.*$



//ADD PRODUCT
router.post('/add/', function(req, res, next){
    Product.create(req.body, function(err, post){
        if(err) return next(err); 
        res.json(post);
    });
});

//UPDATE PRODUCT
router.put('/:_id', function(req, res, next){
    Product.findByIdAndUpdate(req.params._id, req.body, function(err, product){
        if(err) return next(err); 
        res.json(product);
    });
});

router.post('/image', function(req, res, next){
    console.log('./src/assets' + req.body.image);
    fs.unlink('./src/assets/' + req.body.image, (err)=>{
        if(err) console.log(err)
        res.json();
        console.log("product image deleted") 
    })
})

//DELETE PRODUCT
router.delete('/delete/:_id', function(req, res, next){
    Product.findByIdAndDelete(req.params._id, req.body, function(err, post){
        if(err) return next(err); 
        res.json(post)
    })
})

module.exports = router;