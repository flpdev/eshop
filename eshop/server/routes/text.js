var express = require('express');
var router = express.Router();
var Text = require('../models/Text');


//Save text
router.post('/add', function(req, res){
    Text.countDocuments({place: req.body.place}, function(err, count){
        if(count==0){
            Text.create({
                place: req.body.place, 
                text: req.body.text
            }, function(err, newText){
                if(err)  res.send(err); 
                res.json(newText);
            });
        }else{
            Text.findOne({place: req.body.place}, function(err, text){
                if(err) res.send(err);
                text.update({
                    text: req.body.text
                }, function(err){
                    if(err) res.send(err);
                    res.json(text)
                })
            })
        }
    })
})

//GET ALL TEXTS
router.get('/getall', function(req, res, next){
    Text.find(function(err, texts){
        if(err) res.send(err); 
        res.json(texts);
    });
});

//Get one text
router.post('/getOne', function(req, res){
    Text.findOne({place: req.body.place}, function(err, text){
        if(err) res.send(err);
        res.json(text);
    })
})

module.exports = router;