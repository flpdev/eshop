
var express = require('express');
var http = require('http'); 
var fs = require('fs')
var jsonfile = require('jsonfile');
var router = express.Router();
var parseString = require('xml2js').parseString;
var parcel = require('../models/Parcel')
var fixedParcel = require('../models/fixedParcel')

var url = "mongodb://localhost:27017/parcels";
 
// create a client to mongodb
var MongoClient = require('mongodb').MongoClient;

var data = '';
var file ="parcels.json"
var document;

var getParcels = function getParcels(){

    var parcels = [];

                http.get('http://iseteenindus.smartpost.ee/api/?request=destinations&country=EE&type=APT', (resp)=>{
                    resp.on('data', (chunk)=>{
                        data += chunk;
                    }); 
                    
                    resp.on('end', ()=>{
            
                        // console.log(data)
                        parseString(data, function(err, result){
                            // console.log(result)
                            jsonfile.writeFile(file, result);
                            document = result;
                            // jsonfile.readFile(file, function(err, datajson){
                            //     document = datajson;
                            //     if(err) console.log(err);
                            //     console.log(datajson)
                                
                            // })
                            console.log(document)
                            document.destinations.item.forEach(element => {
                                parcels.push(element)
                            });
                            

                            console.log(parcels[0])
            
                            // console.log(parcels)
            
                        })
                    })
                })

    MongoClient.connect(url, function(err, client) {
        if (err) throw err;
        // db pointing to newdb
        
     
        // document to be inserted
        var db = client.db('eshop');
        
        // insert document to 'users' collection using insertOne

        //LOOK IF ALREADY THERE IS A PARCEL LOCATION IN DB, IF THERE IS, DELETE ALL, FETCH AGAIN, IF NOT 
        //ONLY FETCH AND SAVE.
        
        db.collection("parcels").countDocuments((err, count) => {
            console.log(count);
            if(count == 0){
                
                db.collection("parcels").insertMany(parcels, function(err, res) {
                    if (err) throw err;
                    console.log("Document inserted");
                    
                    // close the connection to db when you are done with it
                    client.close();
                });
            }else if (count>0){
                db.collection("parcels").deleteMany({}, function(err, delOk){
                    if(err) throw err; 
                    console.log("Collection dropped"); 
                    if(delOk){
                        db.collection("parcels").insertMany(parcels, function(err, res){      
                            if(err) throw err; 
                            console.log("Document updated")
                        })
                    }
                    client.close();
                }); 


            }
        })

    });

    parcels = [];
}


// var fixParcels = function fixParcels(parcels){
//     var fixedParcels = [];
    
//     parcels.forEach(element => {
//         var fixParcel = new fixedParcel();
//         fixParcel.place_id = element.place_id
//     });
// }
module.exports.getParcels = getParcels;