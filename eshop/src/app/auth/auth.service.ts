import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import {User} from "../models";
import { Router } from '@angular/router';

@Injectable()
export class AuthService {
  authToken: any;
  user: User;

  constructor(
    private http: HttpClient,
    private jwtHelper: JwtHelperService, 
    private router: Router
  ) { }

  tokenGetter () {
    return localStorage.getItem('id_token');
  }

  getUser(){
    return this.http.get<User[]>('users/');
  }
  registerUser(user) {
    return this.http.post<User>('users/register', user);
  }
  authenticateUser(user) {
    return this.http.post<any>('users/authenticate',(user));
  }

  storeUserData(token, user){
    localStorage.setItem('id_token', token);
    localStorage.setItem('user', JSON.stringify(user));
    this.authToken = token;
    this.user = user;
  }

  loadToken(){
    const token = localStorage.getItem('id_token') || null;
    this.authToken = token;
  }

  loggedIn() {
    return this.jwtHelper.isTokenExpired(this.authToken);
  }

  logout() {
    this.authToken = null;
    this.user = null;
    localStorage.clear();
    
  }

}
