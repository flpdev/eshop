import { Component, OnInit } from '@angular/core';
import {Order, Product, smallProduct} from '../models'
import {Router} from '@angular/router'
import {OrderService} from '../shared/services/order.service'
import {CartService} from '../shared/services/cart.service'
import { forEach } from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  //Variables 
  smallProduct: smallProduct = new smallProduct();
  products: Product[] = [];
  smallProducts: smallProduct[]= [];
  totalPrice: number =0;
  totalPriceVat: number = 0;
  theresProducts: boolean = false;
  VAT: number = 0.2;


  constructor(public router: Router, private orderService: OrderService, private cartService: CartService) { }

  ngOnInit() {
    // console.log("cart") //remove later
    //get Products
    this.products = this.cartService.getProducts();
    this.checkIfProducts();
    for (let i = 0; i < this.products.length; i++) {
      this.setPrice(i);
      
    }
    this.setTotalPrice();
  }

  addOne(i: number){ //WHEN + BUTTON OF A PRODUCT CLICKED, BIND countOnOrder value to number
    this.products[i].countOnOrder++;
    this.setPrice(i);
    this.checkIfProducts();
    this.setTotalPrice();
    this.checkIfProducts();
    console.log("price", this.totalPrice) //remove later
  }

  deleteFromCart(i){ //DELETES ONE PRODUCTS FROM THE CART, WHEN X BUTTON CLICKED
    this.products.splice(i,1);
    
    // this.checkIfProducts();
    this.setTotalPrice();
    this.checkIfProducts();
  }

  substractOne(i: number){ //WHEN - BUTTON OF A PRODUCT CLICKED, BIND countOnOrder value to number
    
    if(this.products[i].countOnOrder == 1){ //if there is only one product of this kind
      this.products.splice(i, 1);
      // this.totalPrice = this.totalPrice - this.products[i].price;
      this.setTotalPrice();
      
    }else{
      this.products[i].countOnOrder--;
    }

    this.setPrice(i);
    // this.checkIfProducts();
    this.setTotalPrice();
    this.checkIfProducts();
    console.log("price", this.totalPrice) //remove later
    console.log(this.theresProducts) //remove later
  }

  setPrice(i){ //SETS PRICE OF ONE PRODUCT
    var str;
    if(this.products[i]!= null && this.products[i] !=undefined){
      this.products[i].priceOnCart = this.products[i].price * this.products[i].countOnOrder;
      this.products[i].priceOnCart = parseFloat(this.products[i].priceOnCart.toFixed(2));
      // str = this.products[i].priceOnCart.toString();
      // if(str[str.length-1] == '0'){
      //   str = this.products[i].priceOnCart.toString() +'0';
      //   this.products[i].priceOnCart = parseFloat(str.toFixed(2));
      // }
    }
    
  }

  checkIfProducts(){ //CHECKS IF THERE ARE PRODUCS IN CART
    if(this.products.length == 0){
      this.totalPrice = 0;
      this.totalPriceVat =0;
      this.theresProducts = false;
      (<HTMLInputElement> document.getElementById('checkoutBtn')).disabled = true;
      
    }else{
      this.theresProducts = true;
    }
    
  }

  setTotalPrice(){ //SETS BOTH PRICE AND PRICE WITH VAT
    var str;
    var number: string;
    this.totalPrice = 0;
    this.totalPriceVat = 0;
    if(this.products.length < 1){
      this.totalPrice = 0; //reset to 0 if there is no products
    }else{
      for(let i =0; i<this.products.length; i++){
        this.totalPrice += this.products[i].priceOnCart;

        // number = this.totalPrice.toString();
        // number = number.split('.')[0] + '.' + number.split('.')[1].substring(0,2);
        // this.totalPrice = Number(number);
      }
      this.totalPrice = parseFloat(this.totalPrice.toFixed(2));
      this.totalPriceVat = this.totalPrice;
      this.totalPriceVat = this.totalPriceVat + (this.totalPriceVat * this.VAT);
      this.totalPriceVat = parseFloat(this.totalPriceVat.toFixed(2));
      // str = this.totalPriceVat.toString(); 
      // if(str[str.length-1]=='0'){
      //   str = this.totalPriceVat.toString() + '0'; 
      //   this.totalPriceVat = parseFloat(str.toFixed(2));
      // }else{
        
      // }
      
      

    }

    console.log("prices: ", this.totalPrice, this.totalPriceVat) //remove later
  }

  onSubmit(){ // ON CHECKOUT BUTTON CICKED
    
    console.log("redirectig to checkout") //remove later
    for(let i=0; i<this.products.length; i++){
      console.log(this.products[i]) //remove later
      this.smallProducts.push({_id: this.products[i]._id, countOnOrder: this.products[i].countOnOrder})
    }

    console.log(this.smallProducts) //remove later
    this.orderService.setOrderProducts(this.smallProducts);
    this.orderService.setTotalPrice(this.totalPrice);
    this.router.navigate(['checkout'], {queryParams: {price: this.totalPrice, priceVAT: this.totalPriceVat, currency: 'EUR'}});
  }

  goHome(){ //RETURNS LANDING PAGE
    this.router.navigate(['']);
  }
}
