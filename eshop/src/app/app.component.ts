import { Component } from '@angular/core';

//TESTS
import { ProductService } from './shared/services/product.service';
import {Product} from './models'
import {HttpClient} from '@angular/common/http'
import {OrderService} from './shared/services/order.service'
import {Order, Parcel} from './models'
import {ParcelService} from './shared/services/parcel.service'




//END TESTS

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'eshop';

  constructor(private http: HttpClient, private parcelService: ParcelService, private productService: ProductService, private orderService: OrderService){}
  products: Product[];
  product: Product = new Product(); 
  order: Order = new Order();
  parcels: Parcel[];
  parcel: Parcel = new Parcel();
  str: string;
  ngOnInit(): void {


    //TESTS

    // this.parcelService.getParcels().subscribe(parcels =>{
    //   this.parcels = parcels;
    //   console.log(this.parcels);
    //   this.parcel = parcels[0];
    //   console.log("parcel1: ", this.parcel)
    // })

    // setTimeout(() => {
    //   console.log("Hola");
    //   // this.product._id = '5bf2cd2d03c9225f0447fc89';
    //   this.product.name='test'
    //   var orderNumber = "EE" + String(Math.floor(Math.random()*(9999999999-1)+1));
    //   this.order.orderID = orderNumber;
    //   this.order.address ="testnumber";
    //   // this.order.products.push(this.product);
    //   console.log("Product: ", this.product)
    //   this.productService.addProduct(this.product).subscribe(product =>{
    //     console.log("updated")
    //   }, (err)=>{
    //     console.log(err)
    //   });
    //   this.product.countOnOrder++;
    //   this.order.products.push(this.product);
    //   this.order.parcel = this.parcel;
    //   // this.order.parcel = this.parcel;
    //   this.productService.getProducts().subscribe(products =>{
    //     this.products = products;
    //     // this.order.products.push(this.products[0]);
        
    //   })
    //   // this.order.products[0].countOnOrder++;
    //   this.orderService.saveOrder(this.order).subscribe(order =>{
    //     console.log("order saved: ", order);
    //   }, 
    //   (err)=>{
    //     console.log(err);
    //   })
    // }, 3000);




    //END TESTS
    }
    
}
