import { Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from 'src/app/models';
import { ProductService } from '../../shared/services/product.service';
import {  NgForm } from '@angular/forms';
import { PictureService } from 'src/app/shared/services/picture.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute, 
    private productService: ProductService, 
    private router: Router, private pictureService: PictureService) { }

  //VARIABLES
  product: Product = null;
  id = null;
  filesToSave: File[];
  selectedFiles: File[]
  imagesUploaded: boolean;

  @ViewChild('fileInput') fileInput: ElementRef;
  @ViewChild("eventForm") imgForm: NgForm;

  ngOnInit() {

    //Get product id
    this.activatedRoute.queryParams.subscribe(params =>{
      this.id = params.id
      this.productService.getProductById(this.id).subscribe(product =>{
        if(product == null || product == undefined){
          this.goProducts();
        }else{ 
          this.product = product;
          console.log(this.product) //remove later
        }
      }, err => {
        this.goProducts();
      })
    })
  }

  onFileSelected(event){
    this.filesToSave = [];
    this.selectedFiles = event.target.files;
    if(this.selectedFiles.length > 10){
      confirm("You can only upload 10 files");
      event.target.files = null;
      this.afterUploadImg();
    }

  }

  //RESETS VALUES FATER UPLODAING AN IMAGE TO DB
  afterUploadImg(){
    this.imagesUploaded = false;
    this.fileInput.nativeElement.value ="";
  }

  //RESETS VALUES AFTER UPLOADING PRODUCT TO DB
  afterUpload(){
    this.imgForm.reset();
    this.imagesUploaded = false;
    this.selectedFiles = [];
    this.product = new Product(); 
    this.filesToSave = [];
  }

  //UPLOADS SELECTED IMAGES TO DB
  uploadImages(){ 
    for (let index = 0; index < this.selectedFiles.length && index < 10-this.product.images.length; index++) {
      var fd = new FormData(); 

        fd.append('picture', this.selectedFiles[index], this.selectedFiles[index].name);
         this.pictureService.saveImage(fd)
        .subscribe( res =>{
          this.product.images.push(res.picture)
          console.log("img added: ", res); //remove later
          //this.filesToSave.push(res.picture);
  
        })
    
    }
    this.afterUploadImg();
    this.imagesUploaded = true;

  }

  goProducts(){
    this.router.navigate(['admin/products'])
  }

  //DELETES ONA IMAGE FROM PRODUCT
  deleteImage(image: number){
    let c = confirm("This can't be undone. Are you sure you want to delete the image?");
    if(c=== true){
      console.log(this.product)  //remove later
    var imgName = this.product.images[image];
    this.product.images.splice(image, 1);
    console.log(imgName) //remove later
    console.log(this.product) //remove later
    this.productService.removeImage(imgName).subscribe(res =>{
      console.log(res)//remove later
    })
    }

  }

  //UPDATES PRODUCT TO DB
  updateProduct(){
    let c = confirm("This can't be undone. Are you sure?"); 
    if(c == true){
      this.productService.updateProduct(this.product).subscribe(res =>{
        console.log("product updated") //remove later
        this.router.navigate(['admin/products'])
      })
    }
  }

  // CHANGES MAIN IMAGE FOR PRODUCT
  setMainImage(i){
     if(!!this.product.mainImage){
      var img = this.product.images[i];  
      this.product.images.splice(i, 1, this.product.mainImage)
      this.product.mainImage = img;
      
      console.log(this.product) //remove later
      console.log(this.product.mainImage) //remove later
    }else{
      console.log("here") //remove later
      var img = this.product.images[i];  
      this.product.images.splice(i, 1)
      this.product.mainImage = img;
      console.log(this.product) //remove later
      console.log(this.product.mainImage) //remove later
    }
    
  }

  //DRAGS ONE IMAGE ONE POSITION UP
  imageUp(i){
    if(i != 0){
      var img = this.product.images[i];
      this.product.images[i] = this.product.images[i-1]; 
      this.product.images[i-1] =img;
    }
    
  }

  //DRAGS ONE IMAGE ONE POSITION DOWN
  imageDown(i){
    if(i != this.product.images.length-1){
      var img = this.product.images[i];
      this.product.images[i] = this.product.images[i+1];
      this.product.images[i+1] = img;
    }
  }

}
