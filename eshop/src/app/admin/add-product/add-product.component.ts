import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ProductService } from '../../shared/services/product.service';
import {Product} from '../../models'
import {PictureService} from '../../shared/services/picture.service'
import {  NgForm } from '@angular/forms';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Router } from '@angular/router';



@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  
constructor(private pictureService: PictureService,
  private productService: ProductService, private router: Router) { }


  maxImagesProduct: Number = 10;
  product: Product = new Product();
  products: Product[];
  selectedFiles: File[];
  filesToSave;
  images: string[] = []
  alternativeProducts:string[] = [];
  similarProducts: string[] = [];
  selectedFile: File = null;
  img: string;
  mainImage: boolean = false;
  imgPath: string = '../../../assets/uploads';
  imagesUploaded: boolean=false;

  @ViewChild('fileInput') fileInput: ElementRef;
  @ViewChild("eventForm") imgForm: NgForm;

  
  ngOnInit() {
    this.imagesUploaded = false;
    this.product = new Product();
    this.selectedFiles = [];
    this.filesToSave= [];
    this.images = [];
    
  }

    onFileSelected(event){
      this.filesToSave = [];
      this.selectedFiles = event.target.files;
      if(this.selectedFiles.length > 10){
        confirm("You can only upload 10 files");
        event.target.files = null;
        this.afterUploadImg();
      }

    }

  afterUpload(){
    this.imgForm.reset();
    this.imagesUploaded = false;
    this.selectedFiles = [];
    this.product = new Product(); 
    this.filesToSave = [];
  }

  afterUploadImg(){
    this.imagesUploaded = false;
    this.fileInput.nativeElement.value ="";
  }

   uploadImages(){ 
    for (let index = 0; index < this.selectedFiles.length && index < this.maxImagesProduct; index++) {
      var fd = new FormData(); 

        fd.append('picture', this.selectedFiles[index], this.selectedFiles[index].name);
         this.pictureService.saveImage(fd)
        .subscribe( res =>{
          this.images.push(res.picture);
          this.product.images.push(res.picture)
          console.log("img added: ", res); //remove later
          //this.filesToSave.push(res.picture);
  
        })
    
    }
    this.afterUploadImg();
    this.imagesUploaded = true;

  }

  addProduct(){

    //Getting similar products
    var simProdIds: string[] = [];
    for(let i=0; i<this.similarProducts.length; i++){
      simProdIds.push(this.similarProducts[i].split('=')[1]);
    }
    this.product.similarProducts = simProdIds;

    //getting alternative products
    var altProds: string[] = [];
    for(let i=0; i<this.alternativeProducts.length; i++){
      altProds.push(this.alternativeProducts[i].split('=')[1]);
    }
    this.product.alternativeProducts = altProds;


    if(!!this.imagesUploaded){
      if(!this.product.mainImage){
        confirm("No main image selected")
      }else{
          // this.product.images = this.selectedFiles;
        if(this.product.images.length > 10){
          this.product.images.splice(10, this.product.images.length-10); //LIMIT TO 10 IMAGES 
        }
        console.log('filesToSave: ', this.filesToSave); //remove later
        console.log('productImages: ', this.product.images) //remove later
        
        this.productService.addProduct(this.product)
          .subscribe((product)=>{
            this.afterUpload();
            console.log('New product added: ',product); //remove later
            this.router.navigate(['admin/products'])
          }, 
          (err) => {
            console.log(err); //remove later
          })
      }
     
    }else{
      confirm("No images uploaded");
    }
  }

  //UPDATE PRODUCT
  updateProduct(){
    this.productService.updateProduct(this.product)
      .subscribe(product => {
        this.ngOnInit(); 
      }, 
      (err) =>{
        console.log(err); //remove later
      })
  }



  setMainImage(i){
     if(!!this.mainImage){
      var img = this.product.images[i];  
      this.product.images.splice(i, 1, this.product.mainImage)
      this.product.mainImage = img;
      
      console.log(this.product) //remove later
      console.log(this.product.mainImage) //remove later
     }else{
       this.product.mainImage = this.product.images[i]
       this.product.images.splice(i,1)
       this.mainImage = true;
     }
    
  }

  deleteImage(i: number){
    let c = confirm("Delete image?");
    if(c=== true){
      console.log(this.product) //remove later
      var imgName = this.product.images[i]
      this.product.images.splice(i, 1);
      console.log(imgName) //remove later
      console.log(this.product) //remove later
      this.productService.removeImage(imgName).subscribe(res =>{
        console.log(res) //remove later
    })
    }
  }

  removeMainImage(){
    this.product.images.push(this.product.mainImage); 
    this.product.mainImage =""; 
    this.mainImage = false;
  }

  goProducts(){
      this.router.navigate(['admin/products'])
  }

  imageUp(i){
    if(i != 0){
      var img = this.product.images[i];
      this.product.images[i] = this.product.images[i-1]; 
      this.product.images[i-1] =img;
    }
    
  }

  imageDown(i){
    if(i != this.product.images.length-1){
      var img = this.product.images[i];
      this.product.images[i] = this.product.images[i+1];
      this.product.images[i+1] = img;
    }
  }
}
