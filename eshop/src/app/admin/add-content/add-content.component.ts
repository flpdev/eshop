import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import {Text, AdminPicture, Picture} from '../../models'
import {AdminPictureService} from '../../shared/services/admin-picture.service'; 
import {TextService} from '../../shared/services/text.service'
import {PictureService} from '../../shared/services/picture.service'
import { Router } from '@angular/router';


@Component({
  selector: 'app-add-content',
  templateUrl: './add-content.component.html',
  styleUrls: ['./add-content.component.css']
})
export class AddContentComponent implements OnInit {

  constructor(private adminPictureService:AdminPictureService, 
              private textService: TextService, 
              private pictureService: PictureService, private router: Router) { }

  //VARIABLES
  texts: Text[]; 
  picture = this.picture;
  adminPictures: AdminPicture[] = []; 
  imagesNames: string[] = [];
  text: string;
  file: File = null;
  // imgPath: string = '/assets/uploads/'
  firstPic: string = "";
  secondPic: string = "";
  thirdPic: string = "";
  firstText: string = ""
  secondText: string =""
  thirdText: string =""


  //ADD VIEWCHILDS FOR BUTTONS & IMGS

  //^********* AS FOR THE MOMENT THERE IS NO ESPECIFICATION ON HOW MUCH IMAGES OR TEXTS THE ADMIN SHOULD BE ABLE TO 
  //*********** WORK WITH NOR WHERE WILL BE DISPLAYED THE IMAGES AND TEXT RESPECTIVE VARIABLES ARE NAMED "FIRSTFILE" AND SO ON.
  //*********** THIS CAN BE CHANGED FOR MORE SELFDESCRIPTIVE NAMES ONCE THE DISPLAY LOCATION IS KNOWN.
  
    //IMAGES
    @ViewChild('firstFile') firstFile: ElementRef; 
    @ViewChild('firstButton') firstButton: ElementRef; 
    @ViewChild('secondFile') secondFile: ElementRef;
    @ViewChild('secondButton') secondButton: ElementRef;
    @ViewChild('thirdFile') thirdFile: ElementRef; 
    @ViewChild('thirdButton') thirdButton: ElementRef;
   
    //TEXTS
    @ViewChild('firstTextButton') firstTextButton: ElementRef; 

  ngOnInit() {
    this.getTexts();
    this.getPictures();  
  }

  onFileSelected(event){
    this.file = event.target.files[0];
    if(!this.firstFile.nativeElement.value == false){
      this.firstButton.nativeElement.disabled = false;
    } else if(!this.secondFile.nativeElement.value == false){
      this.secondButton.nativeElement.disabled = false;
    } else if(!this.thirdFile.nativeElement.value == false){
      this.thirdButton.nativeElement.disabled = false;
    } 
  }

  uploadText(place, text){
    this.textService.addText(text, place).subscribe(res =>{
      console.log("text saved"); //remove later
    })
  }

  txtBoxEmpty(){
    if(this.firstText == ""){
      return false;
    }else return true
  }


  nothing(){
    return true;
  }

  reset(place) {
    if(place == 'firstPic'){
      this.firstFile.nativeElement.value = "";
      this.firstButton.nativeElement.disabled= true;
    } else if(place == 'secondPic'){
      this.secondFile.nativeElement.value = "";
      this.secondButton.nativeElement.disabled= true;
    } else if(place == 'thirdPic'){
      this.thirdFile.nativeElement.value = "";
      this.thirdButton.nativeElement.disabled= true;
    }
  }

  onUpload(place: string){
    
    var fd = new FormData();
    fd.append('picture', this.file, this.file.name); 
    this.pictureService.saveImage(fd)
    .subscribe(res =>{
      this.reset(place);
      this.picture = res;
      this.adminPictureService.saveContentPicture(place, this.picture.picture).subscribe(res =>{
        console.log(this.picture.picture) //remove later
        setTimeout(()=>this.getPictures(), 0);
        // var path = this.imgPath+res.picture
        // document.getElementById(place).setAttribute('src', path);
      })
      
    })
    

  }



  getPictures(){
    
    this.adminPictureService.getAllAdminPictures().subscribe((adminpictures) =>{
      adminpictures.forEach(element => {
        if(element.place === 'firstPic'){
          this.firstPic = element.picture
          // console.log(this.firstPic) //remove later
        }
        else if(element.place === 'secondPic'){
          this.secondPic =element.picture
        }
        else if(element.place === 'thirdPic'){
          this.thirdPic = element.picture
        }
      });

    });
  
   
  }

  getTexts(){
    this.textService.getAllTexts().subscribe((text) =>{
      text.forEach(element => {
        if(element.place == 'firstText'){
          this.firstText = element.text;
        }
        else if(element.place == 'secondText'){
          this.secondText = element.text;
        }
        else if(element.place == 'thirdText'){
          this.thirdText = element.text
        }
      });
    })
  }

}