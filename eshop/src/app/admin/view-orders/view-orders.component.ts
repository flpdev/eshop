import { Component, OnInit } from '@angular/core';
import {OrderService} from '../../shared/services/order.service'
import { Order } from 'src/app/models';
import { Router } from '@angular/router';
import { IfStmt } from '@angular/compiler';

@Component({
  selector: 'app-view-orders',
  templateUrl: './view-orders.component.html',
  styleUrls: ['./view-orders.component.css']
})
export class ViewOrdersComponent implements OnInit {

  
  constructor(private orderService: OrderService, private router: Router) { }

  //VARIABLES
  orders: Order[] = [];
  numOrders: number;
  date: Date;
  id: string;
  orderFound: boolean = true;

   /*____________________________________________
   *
   *    ORDERS (this.orders) GET FETCHED IN
   *              NGONINIT FROM DB, 
   *              JUST DISPLAY THEM 
   *            IN FRONTEND AS YOU WISH
   * ____________________________________________*/

  ngOnInit() {
    this.orderFound = true;
    this.orders = []
    this.id = ""
    this.date = new Date();
    this.orders = null; 

    //GET ALL ORDERS FROM DB
    this.orderService.getAllOrders().subscribe(orders =>{
      this.sortByDate(orders)
      this.orders = orders; 
      this.numOrders = orders.length;
    })

  }

  
  //SEARCHES FOR ONE ORDER WITH MARCHING ID
  searchOrder(){
    if(this.id == null || this.id == undefined || this.id == ""){
      this.ngOnInit();
    }
    this.orderService.getOrderByIDReg(this.id).subscribe(res => {
      if(res == null || res == undefined || res.length == 0){
        this.orderFound = false;
      }else{
        console.log(res) //remove later
        this.sortByDate(res);
        this.orders = []
        this.orders = res;
        this.orderFound = true;
      }   
    }, err => {
      return false;
    })
  }

  goDetails(id){
    this.router.navigate(['admin/orderdetails'], {queryParams: {id: id}})
    // var newWindow = window.open('')
  }

  sortByDate(orders: Order[]){
    
      orders.sort(function(a,b){
        if(a.date > b.date){return -1;}
        if(a.date < b.date){return 1;}
      })
  }

}
