import { Component, OnInit } from '@angular/core';
import {OrderService } from '../../shared/services/order.service'
import { ActivatedRoute, Router } from '@angular/router';
import { Order, Parcel, Product } from 'src/app/models';
import {ParcelService} from '../../shared/services/parcel.service'
import { ProductService } from '../../shared/services/product.service';
@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent implements OnInit {

  constructor(private orderService: OrderService,
  private activatedRoute: ActivatedRoute, private router: Router,
  private productService: ProductService) { }

  //VARIABLES
  products: Product[] = []
  id: string;
  order: Order;
  courier: boolean = false
  itella: boolean = false;
  self: boolean = false;
  parcel: Parcel;

  ngOnInit() {
    this.itella= false;
    this.courier = false; 
    this.courier = false;
    this.id = ""

    this.activatedRoute.queryParams.subscribe(params => {
      this.id = params.id
      
      
      this.orderService.gerOrderByID(this.id).subscribe(order =>{
        if(order == null || order == undefined){
          this.router.navigate(['admin/orders'])
        }else{
          this.order = order;
          this.getProducts();
          console.log(this.order) // remove later
          
          if(this.order.sendingMethod == 'courier'){
            this.courier = true;
          }else if(this.order.sendingMethod == 'itella'){
            this.itella = true;
          }else{
            this.self = true;
          }
        }
        
        // console.log(order.parcel[0]) //remove later
      },(err) => {
        console.log(err); //remove later
        this.router.navigate(['admin/orders']);
      })
    })
  }

  goOrders(){
    this.router.navigate(['admin/orders'])
  }

  getProducts(){
    //GET PRODUCTS
      
    for(let i=0; i<this.order.products.length; i++){
      this.productService.getProductById(this.order.products[i]._id).subscribe(res => {
        this.products.push(res);
        this.products[i].countOnOrder = this.order.products[i].countOnOrder;
      })
    }
    console.log(this.products) //remove later
  }

}
