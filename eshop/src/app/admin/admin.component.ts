import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth/auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor(public authService: AuthService, private router: Router, 
    private activatedRoute: ActivatedRoute) { }

  date: Date;

  ngOnInit() {
    // this.router.navigate(['admin/orders'])
    this.date = new Date();
    setInterval(()=>{
      this.date = new Date();
    }, 1000)
    
    //If theres no params and url = /admin, display orders
    this.activatedRoute.queryParams.subscribe(params => {
      if(!params.id){
        if(this.router.url == '/admin'){
          this.router.navigate(['admin/orders'])
        }
       
      }
    })
  }

  onLogoutClick(){
    this.authService.logout();
    this.router.navigate(['']);
  }

  goContent(){
    this.router.navigate(['admin/content'])
  }

  goOrders(){
    this.router.navigate(['admin/orders'])
  }

  goProducts(){
    this.router.navigate(['admin/products'])
  }

  goHome(){
    window.open('/');
  }
}
