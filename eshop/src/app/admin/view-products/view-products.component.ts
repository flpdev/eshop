import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../shared/services/product.service';
import { Product } from 'src/app/models';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view-products',
  templateUrl: './view-products.component.html',
  styleUrls: ['./view-products.component.css']
})
export class ViewProductsComponent implements OnInit {

  constructor(private productService: ProductService, private router: Router) { }

  //VARIABLES
  products: Product[] = []
  keyword2: string
  productfound: boolean = false;

  ngOnInit() {
    this.keyword2 = ""
    this.productfound = true;
    this.products = [];

    this.productService.getProducts().subscribe(products => {
      
      this.products = products
      this.sortByName(this.products)
    })
  }

  //SEARCHES FOR ONE PRODUCT WITH MATCHING NAME AS "KEYWORD" (only full match will prompt results)
  searchProduct(event: any){
    if(!!event.target.value){
      this.keyword2 = event.target.value
      if(this.keyword2 == ""){
        this.ngOnInit();
      }else{
        
        console.log(this.keyword2) //remove later
        this.productService.getProductsByName(this.keyword2).subscribe(res =>{
          console.log(res) //remove later
          if(res == null || res == undefined || res.length == 0){
            this.productfound = false;
            
          }else{
            this.products = res;
            this.sortByName(this.products)
            
            // this.products.push(res)
            this.productfound = true;
          }
        })
      }
    }else{
      this.ngOnInit();
    }
      
 
  }

  //DELETES ONE PRODUCT AND ITS IMAGES
  deleteProduct(_id){
    var product: Product;
    this.productService.getProductById(_id).subscribe(res=> {
      
      if(res != null && res != undefined){
        product = res;
        let c = confirm("Are you sure you want to delete: " + product.name.toUpperCase() + "?")
        if(c === true){
          //DELTE PICTURES FROM PRODUCT

          //First main picture
          this.productService.removeImage(product.mainImage).subscribe(res =>{
            console.log("Main image deleted") //remove later
          })
          //then delete rest of images
          for(let i=0; i<product.images.length; i++){
            this.productService.removeImage(product.images[i]).subscribe(res =>{
              console.log("image deleted") //remove later
              
            })
          }
          //DELETE PRODUCT ITSELF
          this.productService.deleteProduct(_id).subscribe(res =>{
            console.log("product deleted") //remove later
            this.ngOnInit();
          })
        }

      }else{
        alert("Product not found")
      }

    })
    
    
  }

  //SORTS PRODUCTS ALPHABETICALLY (NOT CASE SENSITIVE)
  sortByName(products: Product[]){
    products.sort(function(a,b){
      if(a.name.toLowerCase() < b.name.toLowerCase()){return -1;}
      if(a.name.toLowerCase() > b.name.toLowerCase()){return 1;}
    })
  }

  goProductDetails(id){
    this.router.navigate(['admin/productdetails'] , {queryParams: {id:id}})
  }

  goAddProduct(){
    this.router.navigate(['admin/addProduct'])
  }
}

