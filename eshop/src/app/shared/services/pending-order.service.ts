import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import {Order} from '../../models'


@Injectable({
  providedIn: 'root'
})

export class PendingOrderService {

  constructor(private http: HttpClient) { }

  saveOrder(order: Order){
    return this.http.post<Order>('pendingorder/save', order);
  }

  getOrderById(_id){
    return this.http.get<Order>('pendingorder/' + _id);
  }

  deleteOrder(_id){
    return this.http.delete('pendingorder/delete/' + _id);
  }
}
