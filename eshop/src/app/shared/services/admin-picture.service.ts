import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import {AdminPicture} from '../../models'

@Injectable({
  providedIn: 'root'
})
export class AdminPictureService {

  constructor(private http: HttpClient) { }

  //Save picture
  saveContentPicture(place, picture){
    return this.http.post<AdminPicture>('adminPicture/add', {place: place, picture: picture});
  }

  //Get one picture
  getAdminPicture(place){
    return this.http.post<AdminPicture>('adminPicture/get', {place:place}); 
  }

  getAllAdminPictures(){
    return this.http.get<AdminPicture[]>('adminPicture/getall');
  }
}
