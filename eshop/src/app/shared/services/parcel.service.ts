import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import {Parcel} from '../../models'
@Injectable({
  providedIn: 'root'
})
export class ParcelService {

  constructor(private http: HttpClient) { }

  getParcels(){
    return this.http.get<Parcel[]>('parcels/');
    
  }


}
