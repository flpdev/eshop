import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Picture } from "../../models";

@Injectable()
export class PictureService {

  constructor(
    private http: HttpClient,
  ) { }

  saveImage(fd) {
    return this.http.post<Picture>('picture/add/', fd);
  }

  saveImages(fd) {
    return this.http.post<Picture>('picture/addImages/', fd);
  }

  deteleImage(_id){
    return this.http.delete<Picture>('picture/delete/'+_id);
  }


}

