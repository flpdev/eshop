import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Product } from '../../models';


@Injectable({
  providedIn: 'root'
})

 
export class ProductService {

  constructor(private http: HttpClient) { }

   id: string;
   firstProductId: string;

  //GET ALL PRODUCTS
  getProducts(){
    return this.http.get<Product[]>('product/getall');
  }

  //GET ONE PRODUCT BY ID
  getProductById(_id){
    return this.http.get<Product>('product/' + _id);
  }

  //ADD PRODUCT
  addProduct(product: Product){
    return this.http.post<Product>('product/add', product);
  }

  //UPDATE PRODUCT
  updateProduct(product: Product){
    
    return this.http.put('product/' + product._id, product);

  }

  //DELETE PRODUCT
  deleteProduct(_id){
    return this.http.delete('product/delete/' + _id);
  }

  //GET PRODUCT BY NAME, FOR SEARCH
  getProductsByName(keyword: string){
    return this.http.post<Product[]>('product/byname', {key: keyword})
  }

  //REMOVE PRODUCT IMAGE
  removeImage(image){
    return this.http.post('product/image', {image: image})
  }


}