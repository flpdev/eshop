import { Injectable } from '@angular/core';
import {Text} from '../../models'
import {HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class TextService {

  constructor(private http: HttpClient) { }

  //Add text
  addText(text: string, place){
    return this.http.post<Text>('text/add', {text: text, place: place}); 
  }

  //get text from place
  getTextFromPlace(place){
    return this.http.post<Text>('text/getOne', {place: place})
  }

  getAllTexts(){
    return this.http.get<Text[]>('text/getall');
  }
}
