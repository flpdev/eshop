import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import {Order, Product, smallProduct} from '../../models'


@Injectable({
  providedIn: 'root'
})
export class OrderService {

  //VARIABLES 
  order: Order = new Order;
  IDlength: number = 25;
  possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

  constructor(private http: HttpClient) { }
 
  saveOrder(order: Order){
    return this.http.post<Order>('order/save', order);
  }

  getAllOrders(){
    return this.http.get<Order[]>('order/');
  }

  setOrderProducts(products: smallProduct[]){
    this.order.products = products;
  }

  //GETS ORDER CONTAINING "id" AS REGEX
  getOrderByIDReg(id){
    return this.http.get<Order[]>('order/oneOrderReg/'+ id)
  }

  gerOrderByID(id){
    return this.http.get<Order>('order/oneOrder/'+ id)
  }

  getOrder(){
    //generate order ID
    
    this.order.orderID = 'EE' + this.generateID();
    return this.order;
  }

  
  generateID(): string{
    var id: string ="";
    for(var i=0; i<this.IDlength ; i++){
      id += this.possible.charAt(Math.floor(Math.random() * this.possible.length));
    }

    return id;
  }

  setTotalPrice(price){
    this.order.totalPrice = price;
  }
}
