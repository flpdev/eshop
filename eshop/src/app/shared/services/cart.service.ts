import { Injectable } from '@angular/core';
import { Product } from '../../models'

@Injectable({
  providedIn: 'root'
})
export class CartService {

  products: Product[] = [];

  constructor() { }

  addProduct(product: Product){
    var inList: boolean = false;
    for(let i =0; i<this.products.length; i++){ //Check if it's already in the cart
      if(this.products[i]._id == product._id){ //SEARCH FOR ID, TO BE SURE.
        inList = true; 
        this.products[i].countOnOrder++; // ad one to count if its already in the cart
      }
    }
    if(!inList){
      this.products.push(product); //if not in cart, add it.
    }
  };


  getProducts(){
    return this.products;
  }
}
