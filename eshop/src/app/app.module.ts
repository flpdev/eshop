//Core
import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {FormsModule } from '@angular/forms'
import { registerLocaleData } from '@angular/common';
import localeEt from '@angular/common/locales/et'

registerLocaleData(localeEt, 'et');
//Custom
import { AppComponent } from './app.component';
import { AdminComponent } from './admin/admin.component';
import { MainComponent } from './main/main.component';
import { AppRoutes } from './app.routes';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { JwtModule } from "@auth0/angular-jwt";

//Services
import { ValidateService } from './auth/validate.service';
import { AuthService } from './auth/auth.service';
import { AuthGuard } from './auth/auth.guard';
import { LoginComponent } from './login/login.component';
import { AddProductComponent } from './admin/add-product/add-product.component';
import { ProductViewComponent } from './product-view/product-view.component';
import {PictureService} from './shared/services/picture.service';
import { CartComponent } from './cart/cart.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { AfterOrderComponent } from './after-order/after-order.component'
import {CartService} from './shared/services/cart.service'
import {OrderService} from './shared/services/order.service'
import { AddContentComponent } from './admin/add-content/add-content.component'
import {ParcelService} from './shared/services/parcel.service';
import { LandingComponent } from './landing/landing.component';
import { CheckpaymentComponent } from './checkpayment/checkpayment.component';
import { RegisterComponent } from './register/register.component';
import { ViewProductsComponent } from './admin/view-products/view-products.component';
import { ViewOrdersComponent } from './admin/view-orders/view-orders.component';
import { OrderDetailsComponent } from './admin/order-details/order-details.component';
import { ProductDetailsComponent } from './admin/product-details/product-details.component';
import {ProductService} from './shared/services/product.service'


export function tokenGetter() {
  return localStorage.getItem('id_token');
}


@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    LoginComponent,
    MainComponent,
    AddProductComponent,
    ProductViewComponent,
 
    CartComponent,
    CheckoutComponent,
    AfterOrderComponent,
    AddContentComponent,
    LandingComponent,
    CheckpaymentComponent,
    RegisterComponent,
    ViewProductsComponent,
    ViewOrdersComponent,
    OrderDetailsComponent,
    ProductDetailsComponent,
    
  ],
  imports: [
    RouterModule.forRoot(AppRoutes, {scrollPositionRestoration: 'enabled'}),
    BrowserModule, 
    HttpClientModule,
    FormsModule,
    JwtModule.forRoot({
      config: { tokenGetter: tokenGetter,
      }
    }),
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'et' },
    ValidateService,
    AuthService,
    AuthGuard, 
    PictureService, 
    CartService,
    OrderService,  
    ParcelService,
    ProductService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
