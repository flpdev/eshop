import { Routes, RouterModule } from '@angular/router';

import { MainComponent } from './main/main.component';
import {LoginComponent} from './login/login.component';
import { AdminComponent } from './admin/admin.component';
import { AuthGuard } from './auth/auth.guard';
import { CheckoutComponent } from './checkout/checkout.component';
import { AfterOrderComponent } from './after-order/after-order.component';
import { CartComponent } from './cart/cart.component';
import { ProductViewComponent } from './product-view/product-view.component';
import {AddProductComponent} from './admin/add-product/add-product.component'
import {LandingComponent} from './landing/landing.component'
import {CheckpaymentComponent} from './checkpayment/checkpayment.component'
import {RegisterComponent} from './register/register.component'
import { AddContentComponent} from './admin/add-content/add-content.component'
import { ViewOrdersComponent } from './admin/view-orders/view-orders.component'
import {OrderDetailsComponent} from './admin/order-details/order-details.component'
import {ViewProductsComponent} from './admin/view-products/view-products.component'
import {ProductDetailsComponent} from './admin/product-details/product-details.component'

export const AppRoutes: Routes = [

    { path: 'admin', component: AdminComponent, canActivate: [AuthGuard], 
        children: [
            {path: 'content', component: AddContentComponent, canActivate: [AuthGuard] }, 
            {path: 'addProduct', component: AddProductComponent, canActivate: [AuthGuard] }, 
            {path: 'orders', component: ViewOrdersComponent}, 
            {path: 'orderdetails', component: OrderDetailsComponent},
            {path: 'products', component: ViewProductsComponent},
            {path: 'productdetails', component: ProductDetailsComponent}
            
        ]},

    {
        path: '', component: MainComponent,
        children: [
            {path: '', component: LandingComponent},
            {path: 'checkout', component: CheckoutComponent},
            {path: 'login', component: LoginComponent},
            {path: 'afterOrder', component: AfterOrderComponent}, 
            {path: 'checkPayment', component: CheckpaymentComponent},
            {path: 'cart', component: CartComponent},
            // {path: 'productview', component: CartComponent},
            {path: 'productview', component: ProductViewComponent}, 
            {path: 'register', component: RegisterComponent}
        ]
    },
    {path:'**', redirectTo: '', component: LandingComponent}

];