import { Component, OnInit } from '@angular/core';
import{Router, ActivatedRoute} from '@angular/router'
import {OrderService} from '../shared/services/order.service'
import {Order, Product} from '../models'
import {PendingOrderService} from '../shared/services/pending-order.service'
import { ProductService } from '../shared/services/product.service';



@Component({
  selector: 'app-after-order',
  templateUrl: './after-order.component.html',
  styleUrls: ['./after-order.component.css']
})
export class AfterOrderComponent implements OnInit {

  constructor(
    private orderService: OrderService, private activatedRoute: ActivatedRoute, private router: Router, 
    private pendingOrderService: PendingOrderService, private productService: ProductService) { }

  
  //VARIABLES 
    order: Order = new Order();
    products: Product[] = []
    status: string; 
    orderID: string;
    msg: string;

  ngOnInit() {
    this.products = []
    //GET URL PARAMS
    this.activatedRoute.queryParams.subscribe(params => {
      this.status = params.status;
      this.orderID = params.ref;
      console.log(this.status)//remove later
      console.log(this.orderID)//remove later

      if(this.status == 'COMPLETED' || this.status == 'APPROVED'){
        this.msg = "PAYMENT CORRECT"
        //SAVE ORDER ON DB

        //try to get pendingorder
        this.pendingOrderService.getOrderById(this.orderID).subscribe((order) =>{
          
          if(order == null || order == undefined){
            console.log(this.orderID); //remove later
            //try to get it from real orders
            this.orderService.gerOrderByID(this.orderID).subscribe(order2 =>{
              console.log(order2)     //remove later       
              this.order = order2;
              this.getProducts()
              //if there is no real order, go home;
              if(order2 == null || order2 == undefined){
                this.goToHome();
              }
            })
            // this.router.navigate(['']);
          }else{
            this.order = order; 
            //if pendingOrder doesnt exist
            this.getProducts()
            console.log("here")//remove later
            this.afterFetchOrder();

          }
        })

        

      }else{
        this.msg = 'Oops, something went wrong with the payment, please try again later'
      }
    })

    
  }

  goToHome(){
    this.router.navigate(['']);
  }

  afterFetchOrder(){
    
    //SET PAYED STATE IN ORDER OBJECT
    if(this.status == 'COMPLETED'){
      this.order.status = 'Payed'
    }else if(this.status == 'APPROVED'){
      this.order.status ='Payment approved, in progress';
    }else{
      this.order.status = "Not payed"
    }

    //SAVE ORDER ON DB AND DELETE PENDING ORDER
    this.orderService.saveOrder(this.order).subscribe(res =>{
      console.log("order saved on db")//remove later
      this.pendingOrderService.deleteOrder(this.order.orderID).subscribe(res =>{
        console.log('Pending order deleted', this.order.orderID); //remove later
      })             
    })
  }

  
  print(){
    let printContents, popupWin;
    printContents = document.getElementById('printable').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
}

getProducts(){
  //GET PRODUCTS
    
  for(let i=0; i<this.order.products.length; i++){
    this.productService.getProductById(this.order.products[i]._id).subscribe(res => {
      this.products.push(res);
      this.products[i].countOnOrder = this.order.products[i].countOnOrder;
    })
  }
  console.log(this.products) //remove later
}

}
