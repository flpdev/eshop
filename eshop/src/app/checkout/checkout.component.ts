import { Component, OnInit } from '@angular/core';
import {Order, Payment, Parcel, City} from '../models'
import {OrderService} from '../shared/services/order.service'
import {ParcelService} from '../shared/services/parcel.service'
import{Router, ActivatedRoute} from '@angular/router'
import {PendingOrderService} from '../shared/services/pending-order.service'


@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  order: Order = new Order();
  payment: Payment = new Payment();
  shopID: string= 'e631e61e-5635-4cd8-a995-adaff5e0081f'; //update
  parcels: Parcel[];
  code: number;
  parcel: Parcel; 
  cities: City[] = [];
  citiesnames: string[] = []
  tranportMethod;
  itellaSmartPost: boolean = false;
  showParcel: boolean = false;
  Tallin: City = new City(); 
  Tartu: City = new City();
  price: number;
  priceVAT: number;
  totalPrice: number;
  itellaPrice: number = 5;
  courierPrice: number = 3;
  lastname: string;
  constructor(private orderService: OrderService, private router: Router, private parcelService: ParcelService,
    private activatedRoute: ActivatedRoute, private pendingOrderService: PendingOrderService) { }

  ngOnInit() {
    this.order = this.orderService.getOrder();

    console.log("order: ", this.order) // remove later
    if(this.order.products.length ==0){ //If checkout url is accesed without products, return home
      this.goToHome();
    }
    else{
      //GET URL PARAMS
        this.activatedRoute.queryParams.subscribe(params=>{
        this.price = params.price;
        this.priceVAT = params.priceVAT;
        this.totalPrice = this.priceVAT
        // console.log(this.priceVAT) //remove later
        if(this.price == 0|| this.priceVAT == 0 || this.totalPrice ==0){ //if, somehow anyprice is 0, return home
          this.goToHome();
        }
      })

      
      this.payment.reference = this.order.orderID;

      this.parcelService.getParcels().subscribe(parcels =>{
        this.parcels = parcels;
        
        this.sortByCity(this.parcels);
        this.restoreCityUpperCase(this.parcels);

        //GET ALL CITYS AVAILABLE
        this.getCities();
      
        
      })
    }   
   
  } //END OF NGINIT


  tranportChange(event){
    this.tranportMethod = event.target.value;
    console.log(this.tranportMethod); //Remove later
    if(this.tranportMethod == "itella"){
      this.totalPrice = Number(this.priceVAT) + Number(this.itellaPrice);
      this.totalPrice = parseFloat(this.totalPrice.toFixed(2));
      this.itellaSmartPost = true;
      this.order.sendingMethod ="itella"

    }else if (this.tranportMethod == "self"){
     
      this.showParcel = false;
      this.order.sendingMethod = 'self';
      this.totalPrice = this.priceVAT
      this.order.sendingMethod ="self"
   
    }else if (this.tranportMethod == "courier"){
      
      this.itellaSmartPost = false;
      this.showParcel = false;
      this.totalPrice = Number(this.priceVAT) + Number(this.courierPrice);
      this.totalPrice = parseFloat(this.totalPrice.toFixed(2));
      this.order.sendingMethod = 'courier';
      this.order.sendingMethod ="courier"
      
    }

  }

  //WHEN ITELLA TRNASPORT IS SELECTED AND PARCEL MACHINE SELECTED
  onChange(event){
    this.code = event.target.value
    if(this.code == -1){
      this.showParcel = false;
      this.order.parcel = null;
      
    }else{
      this.showParcel = true;
      this.parcel= this.parcels[this.findParcelByCode(this.code)];
      this.order.sendingMethod = 'itella';
      this.order.parcel = this.parcel;
      // console.clear(); //Remove later
      // console.log(this.parcel); //Remove later
      
      
    }
    console.log("order parcel: ", this.order) //Remove later
  }

  onSubmit(){ //REDIRECT TO PAYMENT GATEWAY
     var date;
    this.order.totalPrice = this.totalPrice;
    console.log(this.payment.reference) //remove later
    
    this.order.date = new Date();
    
    console.log(this.order.date); //remove later
    this.pendingOrderService.saveOrder(this.order).subscribe(res =>{
      console.log("pending order saved") //remove later
    }, err =>{
      console.log(err) //remove later
    })
    this.payment.url = 'https://payment-test.maksekeskus.ee/pay/1//link.html?shop=' + 
    this.shopID +'&amount='+ this.totalPrice + '&reference=' +this.payment.reference +'&country=ee&locale=et';     

    //AT THIS POINT THE ORDER IS SAVED TO /PENDINGORDERS ON DB, LATER IF PAYMENTS I DONE CORRECTLY, 
    //THAT ORDER IS SAVED TO /ORDERS, AND DELETED FROM PENDING ORDERS
  }


  findParcelByName(name: String): number{
    var index: number =0 ;
    for(let i =0; i<this.parcels.length; i++){
      if(this.parcels[0].name == name){
        index = i;
      }
    }
    return index;
  }

  findParcelByCode(code: number): number{
    var parcelFound = false;
    var parcel: Parcel = new Parcel; 
    var index: number;
      for(let i =0; i<this.parcels.length && !parcelFound; i++){
        if(this.parcels[i].place_id[0] == code){
          parcel = this.parcels[0];
          index = i;
          parcelFound = true;
        }
      }

    return index;
  }

  sortByCity(parcels: Parcel[]){
    
    parcels.sort(function(a,b){
      a.city = a.city.toLowerCase();
      b.city = b.city.toLowerCase();
        if(a.city < b.city){return -1;}
        if(a.city > b.city){return 1;}
      })
    
  }

  restoreCityUpperCase(parcels: Parcel[]){
    var c: string;
    for(let i =0; i<parcels.length; i++){
      c = parcels[i].city.slice(0, 1).toUpperCase();
      parcels[i].city= c + parcels[i].city.slice(1, parcels[i].city.length);
    }
  }

  getParcelsByCity(parcels: Parcel[], city: string): Parcel[]{
    var parcelsByCity: Parcel[] = []; 

    for (let i = 0; i < parcels.length; i++) {
      if(parcels[i].city == city){
        parcelsByCity.push(parcels[i]);
      }
      
    }

    return parcelsByCity;
  }


  getCities(){
    for(let i=0; i<this.parcels.length; i++){
      var city: City = new City();
      if(!this.citiesnames.includes(this.parcels[i].city)){
        if(this.parcels[i].city !="Tallinn" && this.parcels[i].city != "Tartu"){
          this.citiesnames.push(this.parcels[i].city);
          city.name = this.parcels[i].city;
          city.parcels = this.getParcelsByCity(this.parcels, this.parcels[i].city);
          this.cities.push(city);
        }
      }
    }

    //ESPECIAL ORDER CITIES
    this.Tallin.name ="Tallinn";
    this.Tallin.parcels = this.getParcelsByCity(this.parcels, this.Tallin.name)
    this.Tartu.name ="Tartu";
    this.Tartu.parcels = this.getParcelsByCity(this.parcels, this.Tartu.name)

    console.log("Tallin: ", this.Tallin)//Remove later
    console.log("Tartu: ", this.Tartu)  //Remove later
  }

  goToCart(){
    this.router.navigate(['cart']);
  }

  goToHome(){
    this.router.navigate([''])
  }
}
