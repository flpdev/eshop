import { Component, OnInit } from '@angular/core';
import { ValidateService } from '../auth/validate.service';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  name: string;
  username: string;
  email: string;
  password: string;

  constructor(private validateService: ValidateService,
    
    private authService: AuthService,
    private router: Router) { }

  ngOnInit() {
  }
  onRegisterSubmit(){ 
    const user = {
      name: this.name,
      email: this.email,
      username: this.username,
      password: this.password
    }
    console.log('onRegisterSubmit user', user); //remove later
    

    // Required fields
    if (!this.validateService.validateRegister(user)) {
      return false;
    }

    // Validate Email
    if (!this.validateService.validateEmail(user.email)) {
      return false;
    }

    // Register user
    this.authService.registerUser(user).subscribe(data => {
      if (data) {
        this.router.navigate(['/login']);
      } else {
        this.router.navigate(['/register']);
      }
    });
  }
}
