import { Component, OnInit } from '@angular/core';
import{Router, ActivatedRoute} from '@angular/router'
import {Payment} from '../models'

@Component({
  selector: 'app-checkpayment',
  templateUrl: './checkpayment.component.html',
  styleUrls: ['./checkpayment.component.css']
})
export class CheckpaymentComponent implements OnInit {


  payment: Payment = new Payment();
  str: string;
  constructor(private activatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {

    this.payment = null;
    this.activatedRoute.queryParams.subscribe(params =>{
      this.str = params.json; 
      this.payment = JSON.parse(this.str);
      console.log(this.str) //remove later
      this.router.navigate(['afterOrder'], {queryParams: {ref: this.payment.reference, status: this.payment.status}})

    })
  }

}
