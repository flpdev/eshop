
export class User {
    static create: any;
    name: string;
    email: string;
    username: string;
    password: string;
}

export class Product{
    static create: any;
    _id: string;
    name: string; 
    shortDesc: string; 
    longDesc: string;
    mainImage: string;
    images: string[] = [];
    video: string; // url?
    size: string;
    features: string[]; 
    materilas: string[];
    quantity: number; 
    price: number; 
    discountPrice: number; 
    stockInfo: number; 
    similarProducts: string[]; 
    alternativeProducts: string[];
    countOnOrder: number = 1;
    priceOnCart: number;
    videoEmbed: string;
}

export class smallProduct{
    static create: any;
    _id: string;
    countOnOrder: number = 1;
}

export class Order{
    static create: any;
    _id: string;
    orderID: string; 
    address: string;
    city: string; 
    ZIPcode: string; 
    date: Date;
    name: string;
    lastname: string; 
    products: smallProduct[] = [];
    details: string;
    totalPrice: number;
    status: string;
    email: string; 
    phone: string;
    paymentMethod: string;
    sendingMethod: string; 
    parcel: Parcel
}

export class Picture {
    static create: any;
    picture: string;
}


export class Payment{
    static create: any; 
    shop: string; 
    amount: number; 
    reference: string; 
    country: string; 
    locale: string;
    url: string;
    status: string;
}

export class Text{
    static create: any; 
    place: string; 
    text: string;
}

export class AdminPicture{
    static create: any; 
    picture: string; 
    place: string;
}
    

export class Parcel{ 
    place_id: number; 
    name: string; 
    city: string; 
    address: string; 
    country: string; 
    postalcode: string;
    routingcode: string; 
    availability: string; 
    description: string; 
    lat: number;
    lng: number;
}

export class City{
    static create: any; 
    name: string; 
    parcels: Parcel[];

}

