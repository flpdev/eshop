import { Component, OnInit, HostListener, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {ProductService} from '../shared/services/product.service'
import {Product, AdminPicture, Text}from '../models'
import { AdminPictureService } from '../shared/services/admin-picture.service';
import { TextService } from '../shared/services/text.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  //VARIABLES
  texts: Text[] = [];
  adminPics: AdminPicture[] = [];
  products: Product[] = [];
  imgPath = "../../assets/uploads"
  constructor(public router: Router, private productService: ProductService, 
    private adminPictureService: AdminPictureService, 
    private textService: TextService) { }

  ngOnInit() {
    // window.scroll(0,0);
    // document.body.scroll(0,0);

    //TESTS
    this.productService.getProducts().subscribe(products =>{
      this.products = products;
    })

    this.adminPictureService.getAllAdminPictures().subscribe(res =>{
      this.adminPics = res;
    })

    this.textService.getAllTexts().subscribe(res => {
      this.texts = res;
    })
    
  }

  //TEST
  goToProduct(id, name:string){
    var nameslashes = name.split(' ').join('_');

   
    this.router.navigate(['productview'], {queryParams:{id: id}} );
    console.log(nameslashes) //remove later
  }

}