import { Component, OnInit } from '@angular/core';
import { DomSanitizer} from '@angular/platform-browser';
import {Product} from '../models'
import {Order} from '../models'

import {ProductService} from '../shared/services/product.service'
import {Router, ActivatedRoute} from '@angular/router'
import {CartService} from '../shared/services/cart.service'

@Component({
  selector: 'app-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.css']
})
export class ProductViewComponent implements OnInit {

  
  constructor(
    private productService: ProductService, 
    private router: Router, 
    private cartService: CartService,
    private sanitizer: DomSanitizer, 
    private activatedRoute: ActivatedRoute) { }

  //VARIABLES
  products: Product[]; 
  order: Order = new Order();
  product: Product = new Product();
  imgUrl: string = "../../assets/uploads"
  id: string;
  name: string;
  firstProduct: Product = new Product();
  mainImg = 'product.mainImage';
  similarProductsIds: string[] =[];
  similarProducts: Product[] =[];
  alternativeProductsIds: string[] = []; 
  alternativeProducts: Product[] = [];
  safeVideoUrl;
  youtubeEmberUrl: String = 'https://www.youtube.com/embed/';


  ngOnInit() {
    
    //GET ID FROM URL PARAMS AND RESTART VALUES
    this.activatedRoute.queryParams.subscribe(params=>{
      this.similarProducts = []; 
      this.similarProductsIds = [];
      this.alternativeProducts = [];
      this.alternativeProductsIds = [];
      this.id = params.id;

      if(this.id==undefined || this.id==null){
        
        this.router.navigate(['']);
      }else{
          //if id correct, get product of id
        this.productService.getProductById(this.id).subscribe((product)=>{
          if(product == null || product == undefined){
            this.router.navigate(['']);
          }
          else{ //IF PRODUCT CORRECT
            this.product = product;
            this.mainImg = product.mainImage
            this.similarProductsIds = product.similarProducts;
            this.alternativeProductsIds = product.alternativeProducts;
    
            //GET SIMILAR PRODUCTS
            if(this.similarProductsIds.length>0){
              for(let i=0; i<this.similarProductsIds.length; i++){
                this.productService.getProductById(this.similarProductsIds[i]).subscribe(product =>{
      
                  this.similarProducts.push(product);
                  
                })
              }
              console.log("similar: ", this.similarProducts) //remove later
            }


            //GET ALTERNATIVE PRODUCTS
            if(this.alternativeProductsIds.length>0){
              for(let i=0; i<this.alternativeProductsIds.length; i++){
                this.productService.getProductById(this.alternativeProductsIds[i]).subscribe(prod => {
                  this.alternativeProducts.push(prod);
                })
              }
              console.log("alternative: ", this.alternativeProducts) //remove later
            }


            this.changeVideoUrl(); // fix embed video url
            this.safeVideoUrl = this.getEmbededVideo(); //make it safe to display
           
          }

      }, err=>{
        this.router.navigate(['']); //Product not available. return home
        console.clear();
      } )
      } 
    })       
  }
  //END OF NGINIT()
  

  changeImage(i){
    this.mainImg = this.product.images[i];
  }

  getEmbededVideo(): string{
    var url; 
      url = this.sanitizer.bypassSecurityTrustResourceUrl(this.product.videoEmbed);
      // console.log(url) //remove later
    return url
  }

  changeVideoUrl(){
      this.product.videoEmbed = this.youtubeEmberUrl +this.product.video.split('=')[1];
  
    // console.log("url: ", this.products[0].videoEmbed) //remove later
  }

  addToCart(id){ 
    // console.log(id) //remove later
    this.productService.getProductById(id).subscribe(product =>{
      this.cartService.addProduct(product);
      console.log("product added to cart:", product.name) //remove later
    })
        
  }

  goToCart(){
    this.router.navigate(['cart'])
  }

  goToHome(){
    this.router.navigate([''])
  }

  goToSimilarProduct(id){
    this.router.navigate(['productview'], {queryParams:{id: id}} );
    // this.ngOnInit();
    // window.scroll(0,0);
  }

  changeToMainImage(){
    this.mainImg = this.product.mainImage;
  }
  


}
